from include import cli_input
from include import read_serial_bytestream
from include import generate_artificial_bytestream

if __name__ == "__main__":
    simulation, port_number, buffer_amount = cli_input.get_selected_record_run_mode()
    if simulation:
        print("Simulation mode selected")
        generate_artificial_bytestream.generate_artificial_bytestream()
    else:
        print("Recording mode selected")
        read_serial_bytestream.record_serial_data(port_number, buffer_amount)

