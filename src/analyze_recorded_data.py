from include import cli_input
from include import plot_and_analye_serial_bytestream
from include import decode_artificial_bytestream

if __name__ == "__main__":
    simulation, threshold = cli_input.get_selected_record_run_mode()
    if simulation:
        print("Simulation mode selected")
        decode_artificial_bytestream.decode_artificial_bytestream(threshold)
    else:
        print("Recording mode selected")
        values = plot_and_analye_serial_bytestream.plot_data()
        plot_and_analye_serial_bytestream.analyze_data(values, threshold)