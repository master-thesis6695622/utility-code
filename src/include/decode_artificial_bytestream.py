import csv

def decode_artificial_bytestream(threshold:int):
    data = []
    time_per_symbol = 5
    decoded_bytestream = []
    with open("artificial_bytestream.csv", newline='') as csvfile:

        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for value in reader:
            data.append(int(value[0]))

    for value in data:
        symbol = []
        average = 0
        counter = 0

        symbol.append(value)
        counter += 1

        if counter == time_per_symbol:
            average = sum(symbol)/time_per_symbol
            counter = 0
            symbol = []
            if average < threshold:
                decoded_bytestream.append(0)
            elif average >= threshold:
                decoded_bytestream.append(1)
