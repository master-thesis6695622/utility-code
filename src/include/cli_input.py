import argparse

def get_selected_record_run_mode():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--simulate', action='store_true', help='Set this flag to simulate the recording process')
    parser.add_argument('-b', '--buffer', type=int, default=10, help='Amount of buffer to read from serial port')
    parser.add_argument('-p', '--port', type=int, default=3, help='COM Port Number to record data from')
    args = parser.parse_args()
    simulate = args.simulate
    port_number = args.port
    buffer_amount = args.buffer

    if port_number < 0:
        raise ValueError("Given port number can't be negative")
    if buffer_amount < 0:
        raise ValueError("Given buffer amount can't be negative")

    return simulate, port_number, buffer_amount

def get_selected_analyze_run_mode():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--simulate', action='store_true', help='Set this analyze the artificial bytestream instead of the recorded data')
    parser.add_argument('-t', '--threshold', type=int, default=3000, help='Set the sample selection threshold')
    args = parser.parse_args()
    simulate = args.simulate
    threshold = args.threshold

    if threshold < 0:
        raise ValueError("Given threshold can't be negative")

    return simulate, threshold