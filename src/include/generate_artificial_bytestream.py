import random
import csv

def generate_artificial_bytestream():
    artificial_bytestream = []

    for bytestream in range(100):
        symbol = random.randint(0,1)
        for symbol_length in range(5):
            if symbol == 0:
                artificial_bytestream.append(symbol + random.randint(10,300))
            elif symbol == 1:
                artificial_bytestream.append(symbol + random.randint(3700,4095))

    with open('artificial_bytestream.csv', 'w', newline='', encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for value in artificial_bytestream:
            writer.writerow([value])