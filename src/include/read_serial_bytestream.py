import serial
import csv

def record_serial_data(port_number:int, amount:int):
    port = f"COM{port_number}"
    # Read serial data from com port with baud rate of 115200
    ser = serial.Serial(port, 115200)
    print(f"{port} opened")

    # Specify the amount of data to read
    print("Reading data")
    data = []
    for index in range(0, amount):
        data.append(ser.readline())

    write_data = []
    for num, buffer in enumerate(data):
        hex_buffer = []
        # discard the first buffer as it might be incomplete
        if num > 0:
            for i in range(0, len(buffer), 2):
                # The 16-bit data is sent in 8-bit bursts, so we need to read 2 bytes at a time to get the full 16-bit value
                received_bytes = buffer[i:i+2]
                # Ignore the carriage return and newline characters
                if received_bytes != b'\x00\x0a':
                    # Reverse the byte order
                    hex_buffer.append("0x" + received_bytes.hex())
            write_data.append(hex_buffer)

    # Write the data to a csv file
    with open('serial_bytestream.csv', 'w', newline='', encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for buffer in write_data:
            for byte_string in buffer:
                writer.writerow([byte_string])

    print("Reading finished")
    print()
    ser.close()
