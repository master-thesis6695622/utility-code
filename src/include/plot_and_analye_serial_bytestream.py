import matplotlib.pyplot as plt
import csv

def plot_data():
    data = []
    values = []
    with open('serial_bytestream.csv', newline='') as csvfile:

        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row, value in enumerate(reader):
            int_value = int(value[0],16)
            if int_value > 0x8000:
                int_value = int_value- 0x8000 # Remove added leading 1
                values.append(int_value)
                int_value = (int_value/4095) * 3.3 # Convert to voltage (3,3V = 4095 ADC steps)
                data.append([row, int_value])

    x = [row[0] for row in data[:20]]
    y = [row[1] for row in data[:20]]

    plt.plot(x, y)
    plt.xlabel('Reading Number')
    plt.ylabel('Voltage (V)')
    plt.title('Data Plot')
    plt.savefig("serial_bytestream.png")

    return values

def reduce_complexity(input_list, threshold):
    result = []
    for item in input_list:
        if item > threshold:
            result.append(item)
    return result

def get_n_max_items(input_list, num_items):
    result = []

    for i in range(0, num_items):
        max_value = 0

        for item in input_list:
            if item > max_value:
                max_value = item

        input_list.remove(max_value)
        result.append(max_value)

    return result

def analyze_data(values, threshold):
    reduced_items = reduce_complexity(values, threshold)
    print(f"Number of analyzed items: {len(reduced_items)}")
    n = 30
    n_max_items = get_n_max_items(reduced_items,n)
    print(f"List of {n} greates items: {n_max_items}")
    mean = sum(n_max_items)/n
    print(f"Mean: {mean}")
    print(f"Median: {n_max_items[int(n/2)]}")
